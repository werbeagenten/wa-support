<?php
/**
 * @package Werbeagenten Support Plugin
 * @version 1.6
 */
/*
Plugin Name: Werbeagenten Support Plugin
Plugin URI: http://werbeagenten.de
Description: Support Plugin Werbeagenten
Author: TW Werbeagenten Heidelberg GmbH
Version: 1.6
Author URI: http://werbeagenten.de/
*/

namespace Werbeagenten\Support;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

$autoloader = __DIR__ . '/vendor/autoload.php';
if (is_readable($autoloader)) :
    include $autoloader;
endif;

/**
* Plugin Class
*/
class Werbeagenten_Customer_Support_Plugin
{

	public static $version = "1.6";

	function __construct()
	{



		new AdminBarCleanup;
		new DashboardCleanup;
		// new DatePicker;
		new DashboardCleanup;
		new DeleteUserFormInfo;
		new MediaLibrary;
		new TincyMceCleanup;
		new SupportPluginUpdate;
		new YoastSeo;
	}

}

new Werbeagenten_Customer_Support_Plugin;
