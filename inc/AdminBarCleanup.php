<?php

namespace Werbeagenten\Support;

defined ( 'ABSPATH' ) or die( 'No script kiddies please!' );


/**
 * Clean up the WordPress Admin Bar
 */
class AdminBarCleanup
{
	
	function __construct()
	{
	
		add_action( 'wp_before_admin_bar_render', array( &$this, 'werbeagenten_support_remove_wp_logo' ) );
	}

	/**
	 * Hide WordPress logo from admin bar
	 */

	public function werbeagenten_support_remove_wp_logo () {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu( 'wp-logo' );
	}
}