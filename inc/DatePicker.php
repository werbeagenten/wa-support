<?php

namespace Werbeagenten\Support;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

//
// Add date picker to publish metabox on post edit screen
//
define( 'PDDP_VERSION', '2.0.1' );
define( 'WERBEAGENTEN_SUPPORT_PLUGIN_DIR', plugin_dir_url( __FILE__ ) );


/**
* 
*/
class DatePicker
{
	
	function __construct()
	{
		add_action( 'admin_enqueue_scripts', array( &$this, 'load_pddp_admin_style_script' ) );
	}


	function load_pddp_admin_style_script( $hook ) {
		if ( 'post.php' != $hook && 'post-new.php' != $hook )
			return;
		
		if ( apply_filters( 'add_pddp_timepicker_js', true ) ) {
			wp_enqueue_script( 'timepicker-js', WERBEAGENTEN_SUPPORT_PLUGIN_DIR . 'js/jquery-ui-timepicker-addon.js', array( 'jquery-ui-datepicker' ) );
		}
		
		if ( apply_filters( 'add_pddp_js', true ) ){
			wp_enqueue_script( 'pddp-js', WERBEAGENTEN_SUPPORT_PLUGIN_DIR . 'js/pddp.js', array( 'timepicker-js' ) );
		}

		if ( apply_filters( 'add_pddp_css', true ) ){
			wp_enqueue_style( 'pddp-css', WERBEAGENTEN_SUPPORT_PLUGIN_DIR . 'css/pddp.css' );
		}
	}

}

