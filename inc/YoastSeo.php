<?php

namespace Werbeagenten\Support;

/**
 *
 * Filter Yoast SEO Metabox Priority
 * @author Jacob Wise
 * @link http://swellfire.com/code/filter-yoast-seo-metabox-priority
 *
 * Set Yoast Metabox prio to low (move below other more imporant metaboxes)
 */
class YoastSeo
{
  function __construct()
  {
    if(! function_exists('wa_filter_yoast_seo_metabox') ){
      add_filter('wpseo_metabox_prio', array($this, 'filter_yoast_seo_metabox'));
    }

    function filter_yoast_seo_metabox() {
      return 'low';
    }
  }
}