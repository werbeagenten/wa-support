<?php

namespace Werbeagenten\Support;

/**
 * Adjustments for the WordPress Media Library
 * 
 * Force List view for media library
 */
class MediaLibrary
{
	
	function __construct()
	{

        add_action( 'admin_init', function() { $_GET['mode'] = 'list'; }, 100 ); // Force media library to list view
        add_action( 'admin_head', array( &$this, 'werbeagenten_remove_grid_view_button') );

	}


    /**
     * Add css into admin to hide deactivated view-grid button
     * @since 1.0
     */
    public function werbeagenten_remove_grid_view_button()
    {
            ?>

            <style type="text/css">
                .view-switch .view-grid {
                    display: none;
                }
            <style>

            <?php
    }

}