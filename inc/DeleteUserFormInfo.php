<?php

namespace Werbeagenten\Support;

class DeleteUserFormInfo {
	function __construct()
	{
		add_action( 'delete_user_form', array( $this, 'extend_delete_user_form' ), 10, 2);
	}


    /**
     *  Show users data (posts) on delete users screen
     */
    function extend_delete_user_form( $current_user, $userids ) {
        echo '<h2>Users content</h2>';

        foreach($userids as $userid){
    
            // get user
            $user = get_user_by('id', $userid);
            echo '<h2>' . $user->get('display_name') . '</h2>';
    
            // get users posts
            $users_posts = get_posts(array(
                'author' => $userid,
                'posts_per_page' => -1,
                'post_type' => 'any' // als post types except with exclude_from_search = true
            ));
            
            // show users post list if we have posts
            if ($users_posts){
    
                echo '<ul>';
                
                foreach($users_posts as $user_post){
                    echo '<li>';
                        echo '<a href="' . get_the_permalink($user_post->ID) .  '">';
                            echo '<strong>';
                            echo $user_post->post_title;
                            echo '</strong>';
                            
                        echo '</a>';
                        echo ' ';
                        echo $user_post->post_type;
                        echo ' ';
                        echo '<a href="' . get_edit_post_link($user_post->ID) . '">Edit</a>';
                    echo '</li>';
                    
                    
                }
                echo '</ul>';
    
            }
    
    
        }
    }
}