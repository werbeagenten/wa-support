<?php

namespace Werbeagenten\Support;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
* Clean up the WordPress Dashboard
*/
class DashboardCleanup
{

	function __construct()
	{

		add_action( 'wp_dashboard_setup', array( &$this, 'werbeagenten_support_dashboard_widgets' ) );

		remove_action( 'welcome_panel', 'wp_welcome_panel' ); // remove welcome panel from dashboard

		add_action( 'wp_dashboard_setup', array( &$this, 'werbeagenten_support_add_dashboard_widgets' ), 1 );

		add_action( 'admin_menu', array( &$this, 'werbeagenten_support_custom_menu_page_removing' ) );

		add_action( 'admin_head', array( &$this,  'werbeagenten_support_hide_update_msg_non_admins' ) );
		
		add_action( 'wp_dashboard_setup', array( &$this,  'remove_wpseo_dashboard_overview' ) );

		add_filter( 'pre_option_show_avatars', array( &$this,  'override_show_avatars' ) );

		add_filter( 'admin_footer_text', array( &$this,  'werbeagenten_custom_backend_footer_text' ) );

		add_action( 'admin_menu', array( &$this,  'werbeagenten_hide_admin_version_number' ) );

		add_filter( 'plugin_action_links_wordpress-seo/wp-seo.php', array( &$this, 'werbeagenten_hide_plugin_links' ), 11 );

		add_filter( 'all_plugins', array( &$this, 'werbeagenten_filter_plugins' ) );

		add_filter( 'plugin_row_meta', array( &$this, 'werbeagenten_hide_plugin_details' ), 10, 2 );

		add_action( 'admin_init', array( $this, 'werbeagenten_add_theme_caps' ) );

		add_action( 'customize_register', array( $this, 'werbeagenten_remove_gotdang_customizer_nags' ), 20 );

		add_action( 'init', array( $this, 'werbeagenten_remove_gotdang_nags' ) );




	}

	/**
	* NOTE: Hide Dasboard Widgets
	*
	* @since 1.0
	*/
	public function werbeagenten_support_dashboard_widgets () {
		global $wp_meta_boxes;

		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

		// register own widget
	}

	/**
	 * NOTE: Add custom Dashboard Widget
	 *
	 * @since 1.0
	 */
	public function werbeagenten_support_add_dashboard_widgets () {

		\wp_add_dashboard_widget( 'wp_dashboard_widget', 'Informationen:', array(&$this, 'werbeagenten_support_detail_dashboard_widget' ) );

	}

 	/**
	 * Add theme info box into WordPress Dashboard
	 *
	 * @since 1.0
	 */
	public function werbeagenten_support_detail_dashboard_widget () {

		echo "<ul>
		<li><strong>Erstellt von:</strong> Werbeagenten Heidelberg</li>
		<li><strong>Website:</strong> <a href='http://www.werbeagenten.de'>www.werbeagenten.de</a></li>
		<li><strong>Kontakt:</strong> <a href='mailto:mail@werbeagenten.de'>mail@werbeagenten.de</a></li>
		</ul>";

	}

	/**
	 * NOTE: Remove menu items from backend
	 * @since 1.0
	 * @since 1.1 Added sucuriscan, wetc, aws, wpmandrill, cloudflare
	 *
	 */
	public function werbeagenten_support_custom_menu_page_removing () {
		if ( !current_user_can('manage_options' ) ) {
			# code...

			// remove_menu_page( 'index.php' );                  //Dashboard
			// remove_menu_page( 'jetpack' );                    //Jetpack*
			//remove_menu_page( 'edit.php' );                   //Posts
			//remove_menu_page( 'upload.php' );                 //Media
			// remove_menu_page( 'edit.php?post_type=page' );    //Pages
			//remove_menu_page( 'edit-comments.php' );          //Comments
			// remove_menu_page( 'themes.php' );                 //Appearance
			// remove_menu_page( 'plugins.php' );                //Plugins
			// remove_menu_page( 'users.php' );                  //Users
			\remove_menu_page( 'tools.php' );                  //Tools
			// \remove_menu_page( 'options-general.php' );        //Settings
			\remove_submenu_page( 'tools.php', 'tools.php' );
			\remove_menu_page( 'sucuriscan' );
			\remove_menu_page( 'w3tc_dashboard' );
			\remove_menu_page( 'amazon-web-services' );
			\remove_submenu_page( 'options-general.php', 'wpmandrill' );
			\remove_submenu_page( 'plugins.php', 'cloudflare' );
		}
	}

	/**
	 * NOTE: Hide update notices for non admins
	 *
	 * @since 1.0
	 */
	public function werbeagenten_support_hide_update_msg_non_admins (){
	     if ( ! \current_user_can( 'manage_options' ) ) { // non-admin users
	        echo '<style>#setting-error-tgmpa>
	        .updated settings-error notice is-dismissible, .update-nag, .updated, #cpto #cpt_info_box { display: none; }</style>';
	    }
	}

	/**
	 * Remove Yoast SEO Dashboard Widget
	 *
	 * @since 1.0
	 */
	public function remove_wpseo_dashboard_overview () {

		\remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );

	}

	/**
	 * NOTE: Hide Avatars
	 *
	 * @since 1.0
	 */
	public function override_show_avatars () {

		global $current_user;

		if ( \current_user_can( 'administrator' ) ) {

		return false;

		}

		return $current_user->show_avatars;

	}

	/**
	 * NOTE: Overwrite the default admin footer text
	 *
	 * @since 1.0
	 */
	public function werbeagenten_custom_backend_footer_text () {
		//echo '&copy; 2016 - <a href="http://www.werbeagenten.de" target="blank">Werbeagenten Heidelberg GmbH</a>';
		echo 'Erstellt von <a href="http://www.werbeagenten.de" target="blank">TW Werbeagenten Heidelberg GmbH</a> | Tel: 06221 193 55 0';
	}

	/**
	 * Hide version number in admin footer from non-admins
	 *
	 * @since 1.0
	 */
	public function werbeagenten_hide_admin_version_number() {
	    if ( ! \current_user_can('werbeagenten_hide_admin_version_number') ) { // 'update_core' may be more appropriate
	        \remove_filter( 'update_footer', 'core_update_footer' );
	    }
	}

	/**
	 * NOTE:  Hide Sensitive Plugins from Plugins Listing
	 *
	 * @since    1.1
	 */
	public function werbeagenten_filter_plugins( $plugins ) {

		$hidden = array(
			'Sucuri Security - Auditing, Malware Scanner and Hardening',
			'W3 Total Cache',
			'Amazon S3 and CloudFront'
		);
		/*if ( !isset($_GET['seeplugins']) || $_GET['seeplugins'] !== 'werbeagenten' ) {
			foreach ($plugins as $key => &$plugin ) {
				if ( in_array( $plugin["Name"], $hidden ) ) {
					unset($plugins[$key]);
				}
			}
		}*/
		return $plugins;
	}

	/**
	 * NOTE:  Hide Awkard Plugin Links
	 *
	 * @since    1.1
	 */
	public function werbeagenten_hide_plugin_links( $links ) {
		// var_dump( "test" );
		// if ( ! empty($links['deactivate']) ) {
		// 	// var_dump($links);
		// 	$links = array(
		// 		'deactivate' => $links['deactivate']
		// 	);
		// }
		return $links;
	}


	/**
	 * NOTE:  Hide Plugin Details
	 *
	 * @since    1.1
	 */
	public function werbeagenten_hide_plugin_details( $links, $file ) {
		$links = array();
		return $links;
	}

	/**
	 * NOTE:  Allow Editors to Get to Widgets and Stuff
	 *
	 * @since    1.1
	 */
	public function werbeagenten_add_theme_caps() {
		$role_object = \get_role( 'editor' );
		$role_object->add_cap( 'edit_theme_options' );
	}

	/**
	 * NOTE:  Remove Customizer Junk
	 *
	 * @since    1.1
	 */
	public function werbeagenten_remove_gotdang_customizer_nags() {
		global $wp_customize;
		$wp_customize->remove_section( get_template() . '_theme_info');
	}

	/**
	 * NOTE:  Remove NAGS (ohgodwhy)
	 *
	 * @since    1.1
	 */
	public function werbeagenten_remove_gotdang_nags() {
		\remove_action( 'admin_notices', 'woothemes_updater_notice' );
	}

}