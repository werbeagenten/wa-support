<?php

namespace Werbeagenten\Support;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
* 
*/
class TincyMceCleanup
{
    
    function __construct()
    {
        add_filter( 'mce_buttons', array( &$this, 'werbeagenten_support_tinymce_buttons' ) );
        add_filter( 'mce_buttons_2', array( &$this, 'werbeagenten_support_tinymce_buttons' ) );

        add_filter( 'tiny_mce_before_init', array( &$this, 'werbeagenten_support_tiny_mce_remove_unused_formats' ) );

        //add_filter( 'tiny_mce_before_init', array( &$this, 'werbeagenten_support_force_adv_buttons' ) );

        add_filter( 'posts_search', array( &$this, 'werbeagenten_support_search_so_14940004' ), 10, 2 );

    }

    /**
    *
     * @since 1.0
     */
    function werbeagenten_support_tinymce_buttons( $buttons )
    {
        # Remove the text color selector
        $remove = array( 'alignleft', 'alignright', 'aligncenter', 'charmap', 'forecolor', 'alignjustify', 'outdent', 'indent', 'strikethrough', 'wp_adv' ); //Add other button names to this array
        # Find the array key and then unset
        return array_diff( $buttons, $remove );
    }

    /**
     *  Modify TinyMCE editor to remove H1.
     *
     * @since 1.0
     */
    function werbeagenten_support_tiny_mce_remove_unused_formats( $init ) {
        // Add block format elements you want to show in dropdown
        $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Address=address;Pre=pre';
        return $init;
    }


    /**
     * Enhance Media Library Search by adding search by filename function
     *
     * @since 1.1
     */

    function werbeagenten_support_search_so_14940004( $search, $a_wp_query ) 
    {
        global $wpdb, $pagenow;

        // Only Admin side && Only Media Library page
        if ( !is_admin() && 'upload.php' != $pagenow ) 
            return $search;

        // Original search string:
        // AND (((wp_posts.post_title LIKE '%search-string%') OR (wp_posts.post_content LIKE '%search-string%')))
        $search = str_replace(
            'AND ((', 
            'AND (((' . $wpdb->prefix . 'posts.guid LIKE \'%' . $a_wp_query->query_vars['s'] . '%\') OR ', 
            $search
        ); 

        return $search;
    }


}
